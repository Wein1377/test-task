FROM gradle:8.5-jdk17 as builder

WORKDIR /home/gradle/src

COPY --chown=gradle:gradle build.gradle settings.gradle gradlew /home/gradle/src/

COPY --chown=gradle:gradle gradle /home/gradle/src/gradle

RUN chmod +x ./gradlew

COPY --chown=gradle:gradle src /home/gradle/src/src

RUN ./gradlew build -x test

FROM openjdk:17

COPY --from=builder /home/gradle/src/build/libs/*.jar /app/test-task.jar

ENTRYPOINT ["java","-jar","/app/test-task.jar"]