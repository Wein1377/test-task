# Действия, необходимые для запуска
***
Перед запуском необходимо создать базу данных PostgreSQL. Для этого необходимо выполнить следующие команды:
<pre>
psql -U "your_postgres_user"

CREATE DATABASE test_task_db;
</pre>
После чего в файле application.properties (src/main/resources/application.properties) необходимо указать данные авторизации для пользователя PostgreSQL, под которым была создана база данных:
<pre>
spring.datasource.username=your_postgres_user
spring.datasource.password=password
</pre>
Далее приложение можно запускать.
# API Endpoints
***
## USER
### Добавление нового клиента
<pre>
POST /api/v1/users
</pre>
<pre>
Request body
{
    "username": "username"
}
</pre>
### Получение списка клиентов
<pre>
GET /api/v1/users/all
</pre>
### Получение информации по заданному клиенту (по id)
<pre>
GET /api/v1/users/{id}
</pre>
<pre>
PathVariable id(Long) - id клиента
</pre>
***
## CONTACT
### Добавление нового контакта клиента (телефон или email)
<pre>
POST /api/v1/contacts/{user_id}
</pre>
<pre>
PathVariable user_id(Long) - id клиента
</pre>
<pre>
Request body 
{ 
    "type": "EMAIL", 
    "value": "nikita0zrz@gmail.com" 
}
</pre>

### Получение списка контактов заданного клиента и списка контактов заданного типа заданного клиента
<pre>
GET /api/v1/contacts/{user_id}/all
</pre>
<pre>
PathVariable user_id(Long) - id клиента
</pre>
<pre>
Request Parameter type(Enum, required = false) - тип контакта
</pre>