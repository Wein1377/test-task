package com.example.test_task.controller;

import com.example.test_task.entity.UserEntity;
import com.example.test_task.exception.UserException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.test_task.service.UserService;

import java.util.List;

@RestController
@RequestMapping("api/v1/users")
public class UserController
{
    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<?> createUser(@Valid @RequestBody UserEntity user)
    {
        try
        {
            return new ResponseEntity<UserEntity>(userService.createUser(user),HttpStatus.CREATED);
        }
        catch (UserException e)
        {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserEntity>> findAll()
    {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOneById(@PathVariable Long id)
    {
        try
        {
            return ResponseEntity.ok(userService.findOneById(id));
        }
        catch (UserException e)
        {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
