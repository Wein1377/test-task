package com.example.test_task.controller;

import com.example.test_task.exception.ContactException;
import com.example.test_task.entity.ContactEntity;
import com.example.test_task.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/contacts")
public class ContactController
{
    @Autowired
    ContactService contactService;

    @PostMapping("/{user_id}")
    public ResponseEntity<?> createContact(@PathVariable Long user_id, @RequestBody ContactEntity contact)
    {
        try
        {
            return new ResponseEntity<ContactEntity>(contactService.createContact(user_id, contact), HttpStatus.CREATED);
        }
        catch (ContactException e)
        {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{user_id}/all")
    public ResponseEntity<?> findContacts(@PathVariable Long user_id,
                                          @RequestParam (required = false) ContactEntity.ContactType type)
    {
        try
        {
            return ResponseEntity.ok(contactService.findContacts(user_id, type));
        }
        catch (ContactException e)
        {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
