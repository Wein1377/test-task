package com.example.test_task.exception;

public class ContactException extends Exception
{
    public ContactException(String message)
    {
        super(message);
    }
}
