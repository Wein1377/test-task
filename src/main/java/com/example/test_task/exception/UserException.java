package com.example.test_task.exception;

public class UserException extends Exception
{
    public UserException(String message)
    {
        super(message);
    }
}
