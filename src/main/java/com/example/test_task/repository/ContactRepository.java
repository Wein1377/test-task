package com.example.test_task.repository;

import com.example.test_task.entity.ContactEntity;
import com.example.test_task.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContactRepository extends JpaRepository<ContactEntity, Long>
{
    ContactEntity findByValue(String value);
    ContactEntity findByValueAndUser(String value, UserEntity user);
    List<ContactEntity> findByUser(UserEntity user);
    List<ContactEntity> findByUserAndType(UserEntity user, ContactEntity.ContactType type);
}
