package com.example.test_task.service;

import com.example.test_task.exception.ContactException;
import com.example.test_task.entity.ContactEntity;
import com.example.test_task.entity.UserEntity;
import com.example.test_task.repository.ContactRepository;
import com.example.test_task.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class ContactService
{
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private UserRepository userRepository;
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static final Pattern VALID_NUMBER_REGEX =
            Pattern.compile("^([+][7]|[7]|[8])[\\d\\-\\(\\) ]{9,14}$", Pattern.CASE_INSENSITIVE);
    public ContactEntity createContact(Long user_id, ContactEntity contact) throws ContactException
    {
        Optional<UserEntity> user = userRepository.findById(user_id);
        if(user.isEmpty())
        {
            throw new ContactException("Пользователь не существует");
        }
        if(contact.getType() == ContactEntity.ContactType.EMAIL &&
                !VALID_EMAIL_ADDRESS_REGEX.matcher(contact.getValue()).matches())
        {
            throw new ContactException("Почта указана в неверном формате");
        }
        else if(contact.getType() == ContactEntity.ContactType.PHONE)
        {
            if(!VALID_NUMBER_REGEX.matcher(contact.getValue()).matches())
            {
                throw new ContactException("Номер указан в неверном формате");
            }
            else
            {
                String phone = contact.getValue();
                if(phone.startsWith("+7"))
                {
                    phone = "8".concat(phone.substring(2));
                    contact.setValue(phone.replaceAll("(\\W)", ""));
                }
                else if(phone.startsWith("7"))
                {
                    phone = "8".concat(phone.substring(1));
                    contact.setValue(phone.replaceAll("(\\W)", ""));
                }
                else
                {
                    contact.setValue(phone.replaceAll("(\\W)", ""));
                }
            }
        }
        if(contactRepository.findByValueAndUser(contact.getValue(), user.get()) != null)
        {
            throw new ContactException("У заданного пользователя уже есть такой контакт");
        }
        contact.setUser(user.get());
        return contactRepository.save(contact);
    }

    public List<ContactEntity> findContacts(Long user_id, ContactEntity.ContactType type) throws ContactException
    {
        Optional<UserEntity> user = userRepository.findById(user_id);
        if(user.isEmpty())
        {
            throw new ContactException("Пользователь не существует");
        }
        if(type != null)
        {
            return contactRepository.findByUserAndType(user.get(), type);
        }
        else
        {
            return contactRepository.findByUser(user.get());
        }
    }
}
