package com.example.test_task.service;

import com.example.test_task.entity.UserEntity;
import com.example.test_task.exception.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.test_task.repository.UserRepository;

import java.util.List;

@Service
public class UserService
{
    @Autowired
    private UserRepository userRepository;

    public UserEntity createUser(UserEntity user) throws UserException
    {
        if(userRepository.findByUsername(user.getUsername()) != null)
        {
            throw new UserException("Такой пользователь уже существутет");
        }
        return userRepository.save(user);
    }

    public List<UserEntity> findAll()
    {
        return userRepository.findAll();
    }

    public UserEntity findOneById(Long id) throws UserException
    {
        UserEntity user = userRepository.findById(id).get();
        if(user == null)
        {
            throw new UserException("Пользователья не найден");
        }
        return user;
    }
}
